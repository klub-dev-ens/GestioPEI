# GestioPEI

Coordination and data centralization for PEI (Programme Étudiant·e Invité·e),
as a Django website.


## Développement

*[Recommandé]* Créer un environnement virtuel Python 3.

    virtualenv -p python3 venv

Installer les dépendances du projet.

    pip install -U -r requirements-dev.txt

Exécuter la commande suivante pour créer une base de données pré-remplie.
Si vous avez modifié des migrations, supprimer préalablement le fichier
`db.sqlite3`.

    ./manage.py preparedjango

Lancer le serveur de développement :

    ./manage.py runserver

Rendez-vous dans votre navigateur à <http://localhost:8000>.


## SCSS

Pour modifier le CSS, ne pas toucher directement aux fichier `.css` mais plutôt aux fichier `.scss`. Il faut installer l'outil `sass` (ou `ruby-sass` sur certaines distributions) et lancer `sass --watch global.scss:global.css`.

Tous les autres fichiers sont chargés dans le fichier `global.scss`.
