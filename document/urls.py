from django.conf.urls import url

from . import views


app_name = "document"
urlpatterns = [
    url(r"^list", views.documents_list, name="list"),
]
