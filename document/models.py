from django.db import models
from django.utils.translation import ugettext_lazy as _


class Tag(models.Model):
    name = models.CharField(
        verbose_name=_("nom du tag"),
        max_length=64
    )

    class Meta:
        verbose_name = _("tag")
        verbose_name_plural = _("tags")

    def __str__(self):
        return self.name


class Document(models.Model):
    title = models.CharField(
        _("titre"),
        max_length=255
    )
    file = models.FileField(
        verbose_name=_("fichier"),
        upload_to="files/"
    )
    tags = models.ManyToManyField(
        Tag,
        verbose_name=_("tags"),
    )

    class Meta:
        verbose_name = _("document")
        verbose_name_plural = _("documents")

    def __str__(self):
        return self.title
