from django.contrib import admin

from .models import Tag, Document


admin.site.register(Tag)
admin.site.register(Document)
