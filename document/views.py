from django.views.generic import ListView

from user.mixins import ApprovalRequiredMixin

from .models import Document, Tag


class DocumentsList(ApprovalRequiredMixin, ListView):
    """
    List all (tagged) documents
    - One can filter on one tag (and only one)
    - If the list is filtered on a tag, the link to the full list appears in
      the template
    """
    model = Document
    template_name = "document/list.html"
    context_object_name = "documents"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["filtered_on_tag"] = self.request.GET.get("tag", None)
        context["tags"] = Tag.objects.all()
        return context

    def get_queryset(self):
        qs = Document.objects.prefetch_related("tags")
        if "tag" in self.request.GET:
            return qs.filter(tags__name__in=[self.request.GET["tag"]])
        return qs

documents_list = DocumentsList.as_view()
