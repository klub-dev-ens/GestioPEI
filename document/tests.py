from django.core.files import File
from django.test import Client, TestCase

from user.models import GuestStudent, Tutor, Volunteer

from .models import Document, Tag


class DocTestMixin:
    file = None

    def setUp(self):
        self.g = GuestStudent.objects.create(username="g")
        self.t = Tutor.objects.create(username="t")
        self.v = Volunteer.objects.create(username="v")

        self.c_g = Client()
        self.c_g.force_login(self.g)
        self.c_t = Client()
        self.c_t.force_login(self.t)
        self.c_v = Client()
        self.c_v.force_login(self.v)

    def create_doc(self, title, tags):
        if not self.file:
            self.file = File(open("README.md"), "README")
        doc = Document.objects.create(title=title, file=self.file)
        doc.tags.add(*tags)
        return doc


class DocumentListTest(DocTestMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.url = "/document/list"

        tag_foo = Tag.objects.create(name="foo")
        tag_bar = Tag.objects.create(name="bar")

        self.create_doc(title="foodoc", tags=[tag_foo])
        self.create_doc(title="foobardoc", tags=[tag_foo, tag_bar])
        self.create_doc(title="bardoc", tags=[tag_bar])

    def test_access(self):
        # Not approved => not allowed to get documents
        r_v = self.c_v.get(self.url)
        self.assertRedirects(
            r_v, "/auth/login/?next={}".format(self.url),
            fetch_redirect_response=False
        )
        self.v.approved = True
        self.v.save()
        self.v.volunteer.refresh_from_db()
        # Now v can get the list
        r_v = self.c_v.get(self.url)
        self.assertEqual(r_v.status_code, 200)

        r_g = self.c_g.get(self.url)
        self.assertEqual(r_g.status_code, 200)
        r_t = self.c_t.get(self.url)
        self.assertEqual(r_t.status_code, 200)

    def test_get_list(self):
        # All documents
        resp = self.c_g.get(self.url)
        self.assertQuerysetEqual(
            Document.objects.order_by("id"),
            map(repr, resp.context_data["documents"].order_by("id"))
        )

        # Filter on one tag
        resp = self.c_g.get("{}?tag=foo".format(self.url))
        self.assertQuerysetEqual(
            Document.objects.filter(tags__name__in=["foo"]).order_by("id"),
            map(repr, resp.context_data["documents"].order_by("id"))
        )


class DocumentDLTest(DocTestMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.doc = self.create_doc(title="example_doc", tags=[])

    def test_direct_access(self):
        url = self.doc.file.url
        # Not approved => not allowed to get documents
        r_v = self.c_v.get(url)
        self.assertRedirects(
            r_v, "/auth/login/?next={}".format(url),
            fetch_redirect_response=False
        )
        self.v.approved = True
        self.v.save()
        self.v.volunteer.refresh_from_db()
        # Now v can get the list
        r_v = self.c_v.get(url)
        self.assertEqual(r_v.status_code, 200)

        r_g = self.c_g.get(url)
        self.assertEqual(r_g.status_code, 200)
        r_t = self.c_t.get(url)
        self.assertEqual(r_t.status_code, 200)
