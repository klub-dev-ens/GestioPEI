from django.core.management import call_command, BaseCommand


class Command(BaseCommand):
    help = "Load development data."

    def handle(self, *args, **options):
        call_command('loaddata', 'positions')
        call_command('loaddata', 'user_dev')
        call_command('loaddata', 'shared_dev')
        call_command('syncmails', 'shared/fixtures/custommail.json')
