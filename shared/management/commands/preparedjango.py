from django.core.management import call_command, BaseCommand


class Command(BaseCommand):
    help = "Setup Django for development."

    def handle(self, *args, **options):
        call_command('migrate')
        call_command('loaddevdata')
