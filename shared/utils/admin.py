from django import forms
from django.contrib import admin
from django.db import models


class ModelAdmin(admin.ModelAdmin):
    """
    ModelAdmin with better defaults.

    # ManyToManyField

    Default widget for these fields is set to CheckboxSelectMultiple, which is
    better when there are only a few choices.

    For fields with larger set of choices, one should use the filter_horizontal
    or filter_vertical attributes of ModelAdmin.

    """
    formfield_overrides = {
        models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple},
    }
