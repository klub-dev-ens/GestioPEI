from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import StringPreference


@global_preferences_registry.register
class CalendarId(StringPreference):
    name = 'calendar_id'
    default = ''
    help_text = (
        "Pour obtenir l'ID de l'agenda, allez dans les "
        "<a href=\"https://calendar.google.com/calendar/r/settings\">"
        "<tt>paramètres de Google Calendar</tt></a>. Sous <tt>Paramètres de "
        "mes agendas</tt>, sélectionnez le calendrier à partager et récupérez "
        "la valeur de <tt>ID de l'agenda</tt> dans la section <tt>Intégrer "
        "l'agenda</tt>. Assurez vous également que le calendrier est public."
        "<br>"
        "Vous pouvez donner plusieurs IDs séparés par des ;."
    )
