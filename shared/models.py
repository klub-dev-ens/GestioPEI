from django.db import models
from django.utils.translation import ugettext_lazy as _


class Language(models.Model):
    name = models.CharField(
        _("nom"),
        max_length=255,
    )

    class Meta:
        verbose_name = _("langue")
        verbose_name_plural = _("langues")

    def __str__(self):
        return self.name


class Subject(models.Model):
    name = models.CharField(
        _("nom"),
        max_length=255,
    )

    class Meta:
        verbose_name = _("discipline")
        verbose_name_plural = _("disciplines")

    def __str__(self):
        return self.name
