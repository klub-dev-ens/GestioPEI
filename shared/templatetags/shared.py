from urllib.parse import urlencode

from django import template
from django.utils.safestring import mark_safe

from dynamic_preferences.registries import global_preferences_registry

register = template.Library()


def googlecalendar_iframe(id_):
    src = 'https://calendar.google.com/calendar/embed?' + urlencode({
        'height': 600,
        'wkst': 2,
        'bgcolor': '#FFFFFF',
        'src': id_,
        'color': '#1B8871',
        'ctz': 'Europe/Paris',
        'showTitle': 0,
    })

    return mark_safe(
        '<iframe src="{src}" style="border-width:0" width="100%" '
        'height="600" frameborder="0" scrolling="no"></iframe>'
        .format(src=src)
    )


@register.inclusion_tag('shared/calendar.html', takes_context=True)
def show_calendar(context):
    preferences = global_preferences_registry.manager()

    calendar_id = preferences['calendar_id']
    if calendar_id:
        html = googlecalendar_iframe(preferences['calendar_id'])
    else:
        html = ''

    return {
        'html': html,
        'user': context['user'],
    }
