from django.contrib import admin
from django.contrib.sites.models import Site

from .models import Language, Subject

admin.site.register(Language)

admin.site.register(Subject)

admin.site.unregister(Site)
