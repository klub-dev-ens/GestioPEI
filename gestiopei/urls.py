from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import reverse_lazy
from django.views.generic import RedirectView
from django.views.static import serve

from allauth_ens.views import capture_login, capture_logout

from user.decorators import approvement_required

redirect_home_view = RedirectView.as_view(url=reverse_lazy('home'))

auth_overrides_urls = [
    # Catch login/logout views of admin site.
    url(r'^admin/login/$', capture_login),
    url(r'^admin/logout/$', capture_logout),
    # Deactivate emails management (which comes from allauth).
    url(r'^profil/email/$', redirect_home_view),
]

urlpatterns = auth_overrides_urls + [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('pei.urls')),
    url(r'^', include('user.urls')),
    url(r'^auth/', include('allauth.urls')),
    url(r'^document/', include('document.urls')),
]

urlpatterns += [
    url(
        r"^media/(?P<path>.*)$", approvement_required(serve),
        {"document_root": settings.MEDIA_ROOT}
    )
]


# Django Debug Toolbar
# http://django-debug-toolbar.readthedocs.io/en/stable/installation.html

if "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__', include(debug_toolbar.urls)),
    ] + urlpatterns
