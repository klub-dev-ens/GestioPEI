"""
Django base settings for gestiopei project.

Everything which is supposed to be identical between the production server and
the local development server should be here.
"""


import os
import sys

from django.contrib.messages import constants as messages

try:
    from . import secret
except ImportError:
    raise ImportError(
        "The secret.py file is missing.\n"
        "For a development environment, simply copy secret_example.py"
    )


def import_secret(name):
    """
    Shorthand for importing a value from the secret module and raising an
    informative exception if a secret is missing.
    """
    try:
        return getattr(secret, name)
    except AttributeError:
        raise RuntimeError("Secret missing: {}".format(name))


SECRET_KEY = import_secret("SECRET_KEY")
ADMINS = import_secret("ADMINS")
SERVER_EMAIL = import_secret("SERVER_EMAIL")
EMAIL_HOST = import_secret("EMAIL_HOST")

DBNAME = import_secret("DBNAME")
DBUSER = import_secret("DBUSER")
DBPASSWD = import_secret("DBPASSWD")

BASE_DIR = os.path.dirname(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
)

TESTING = sys.argv[1] == 'test'

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',

    'custommail',
    'dynamic_preferences',
    'widget_tweaks',

    'allauth_ens',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth_cas',

    'allauth_ens.providers.clipper',

    'crispy_forms',

    'user',
    'pei',
    'shared',
    'document',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
]

ROOT_URLCONF = 'gestiopei.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'gestiopei.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DBNAME,
        'USER': DBUSER,
        'PASSWORD': DBPASSWD,
        'HOST': os.environ.get('DBHOST', 'localhost'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

BASE_DJ_PWD_VALID_PATH = 'django.contrib.auth.password_validation'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': BASE_DJ_PWD_VALID_PATH + '.UserAttributeSimilarityValidator',
    },
    {
        'NAME': BASE_DJ_PWD_VALID_PATH + '.MinimumLengthValidator',
    },
    {
        'NAME': BASE_DJ_PWD_VALID_PATH + '.CommonPasswordValidator',
    },
    {
        'NAME': BASE_DJ_PWD_VALID_PATH + '.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = "user.User"


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

MEDIA_URL = "/media/"
MEDIA_ROOT = "media"

STATIC_URL = '/static/'

# Misc settings

SITE_ID = 1

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

MAIL_DATA = {
    'default': {
        'FROM': 'pei@ens.fr',
    },
}

# Authentication
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth
# https://django-allauth.readthedocs.io/en/latest/index.html
# https://git.eleves.ens.fr/cof-geek/django-allauth-ens

AUTHENTICATION_BACKENDS = [
    'allauth.account.auth_backends.AuthenticationBackend',
]

LOGIN_URL = 'account_login'
LOGIN_REDIRECT_URL = 'pei:home'
ACCOUNT_LOGOUT_REDIRECT_URL = 'pei:home'

ACCOUNT_ADAPTER = 'user.adapter.AccountAdapter'
ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = False
ACCOUNT_DETAILS_URL = 'user:profile'
ACCOUNT_HOME_URL = 'pei:home'
ACCOUNT_USER_DISPLAY = lambda u: u.get_full_name() or u.username

SOCIALACCOUNT_PROVIDERS = {
    "clipper": {
        'MESSAGE_SUGGEST_LOGOUT_ON_LOGOUT': True,
        'MESSAGE_SUGGEST_LOGOUT_ON_LOGOUT_LEVEL': messages.INFO
    }
}

CRISPY_TEMPLATE_PACK = 'bootstrap3'
