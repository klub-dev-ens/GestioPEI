"""
Django local settings for the gestiopei project.
The settings that are not listed here are imported from .base
"""

import os

from .base import *  # NOQA
from .base import INSTALLED_APPS, MIDDLEWARE, TESTING, BASE_DIR


# Use sqlite for local development
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3")
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEBUG = True

if TESTING:
    PASSWORD_HASHERS = [
        'django.contrib.auth.hashers.MD5PasswordHasher',
    ]


def show_toolbar(request):
    """
    On ne veut pas la vérification de INTERNAL_IPS faite par la debug-toolbar
    car cela interfère avec l'utilisation de Vagrant. En effet, l'adresse de la
    machine physique n'est pas forcément connue, et peut difficilement être
    mise dans les INTERNAL_IPS.
    """
    return DEBUG


if not TESTING:
    INSTALLED_APPS += ["debug_toolbar"]

    MIDDLEWARE = [
        "debug_toolbar.middleware.DebugToolbarMiddleware"
    ] + MIDDLEWARE

    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_TOOLBAR_CALLBACK': show_toolbar,
    }
