"""
Django development settings for the cof project.
The settings that are not listed here are imported from .base
"""

import os

from .base import *  # NOQA
from .base import BASE_DIR, import_secret


DEBUG = False

ALLOWED_HOSTS = [
    "pei2.ens.fr"
]


STATIC_ROOT = os.path.join(
    os.path.dirname(BASE_DIR),
    "public",
    "gestion",
    "static",
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

STATIC_URL = "/gestion/static/"
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")
MEDIA_URL = "/gestion/media/"
