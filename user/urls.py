from django.conf.urls import url

from . import views

app_name = 'user'
urlpatterns = [
    url(r'^profil/$', views.profile_view,
        name='profile'),
    url(r'^profil/edition/$', views.profile_edit_view,
        name='profile.edit'),
    url(r'^profil/(?P<username>\w+)/$', views.profile_view,
        name='profile.show'),
]
