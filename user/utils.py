from django.conf import settings
from django.db.models import Q

from custommail.shortcuts import send_mass_custom_mail

from .models import GuestStudent, Volunteer


def send_mail_contact_change(changed_user):

    notif_users = get_recipients_contact_change(changed_user)

    mail_name = 'user-change-contact'
    mail_from = settings.MAIL_DATA['default']['FROM']
    datatuple = [
        (
            mail_name,
            {
                'user': user,
                'changed_user': changed_user,
            },
            mail_from,
            [user.email],
        )
        for user in notif_users
    ]

    send_mass_custom_mail(datatuple)


def get_recipients_contact_change(user):
    """
    Returns the list of users to notify about `user` contact changes.

    If `user` is a guest student:
        [binome, tandem]
    If `user` is a tutor or a volunteer:
        [his guest students] + [theirs volunteers]
    Else nobody.
    """
    if user.is_gueststudent:

        # Strange bug if this is removed…
        if not isinstance(user, GuestStudent):
            user = user.gueststudent

        notifs = []
        if user.binome:
            notifs.append(user.binome)
        if user.tandem:
            notifs.append(user.tandem)
        return notifs

    if user.is_volunteer or user.is_tutor:
        # Get the guest students of `user`.
        gss = GuestStudent.objects.all()
        if user.is_volunteer:
            vol = user.volunteer
            gss = gss.filter(Q(binome=vol) | Q(tandem=vol))
        else:
            tut = user.tutor
            gss = gss.filter(Q(tuteur=tut))

        # And theirs own related volunteers.
        vols = Volunteer.objects.of_gueststudents(gss).exclude(pk=user.pk)

        return list(gss) + list(vols)

    return []
