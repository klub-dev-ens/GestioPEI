from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from user.admin import PositionFilter, VolunteerAdmin
from user.models import GuestStudent, Volunteer

User = get_user_model()


class VolunteerAdminTests(TestCase):

    def setUp(self):
        superuser = User.objects.create_superuser('root', '', '')
        self.client.force_login(superuser)

        self.vol1 = Volunteer.objects.create(username='vol1')
        self.vol2 = Volunteer.objects.create(username='vol2')
        self.vol3 = Volunteer.objects.create(username='vol3')

        GuestStudent.objects.create(
            username='gs1', binome=self.vol1, tandem=self.vol2)
        GuestStudent.objects.create(
            username='gs2', binome=self.vol1)
        GuestStudent.objects.create(
            username='gs3', binome=self.vol2, tandem=self.vol2)

    def get_results(self, parameters):
        url = reverse('admin:user_volunteer_changelist')
        resp = self.client.get(url, parameters)
        return resp.context['cl'].queryset

    def test_filter_positions_binome(self):
        self.assertQuerysetEqual(
            self.get_results({'positions': 'binome'}),
            map(repr, [self.vol1, self.vol2]),
        )

    def test_filter_positions_tandem(self):
        self.assertQuerysetEqual(
            self.get_results({'positions': 'tandem'}),
            map(repr, [self.vol2]),
        )

    def test_filter_positions_none(self):
        self.assertQuerysetEqual(
            self.get_results({'positions': 'none'}),
            map(repr, [self.vol3]),
        )
