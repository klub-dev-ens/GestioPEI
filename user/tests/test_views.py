from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.test import Client, TestCase
from django.urls import reverse

from shared.models import Subject

from user.decorators import is_approved_user
from user.forms import GuestStudentForm, TutorForm, VolunteerForm
from user.models import GuestStudent, Tutor, Volunteer

User = get_user_model()


class ProfileEditViewTests(TestCase):

    def setUp(self):
        self.url = '/profil/edition/'

        self.subject = Subject.objects.create(name='The Subject')

    # Utils.

    def assertGetOk(self, resp, form_class):
        self.assertEqual(resp.status_code, 200)
        self.assertIsInstance(resp.context['form'], form_class)
        self.assertTemplateUsed(resp, 'user/profile_edit.html')

    def assertPostOk(self, resp):
        if resp.status_code == 200:
            msg = "[DEBUG] Error(s) in 'form':\n%(errors)s\n" % {
                'errors': resp.context['form'].errors.as_text(),
            }
        else:
            msg = ''

        self.assertRedirects(resp, '/profil/', msg_prefix=msg)

    # Basic tests for this view.

    def test_url(self):
        self.assertEqual(self.url, reverse('user:profile.edit'))

    def test_forbidden(self):
        """
        Unauthenticated user can't access the page.
        """
        r = Client().get(self.url)
        self.assertRedirects(
            r, '/auth/login/?next={}'.format(self.url),
            fetch_redirect_response=False,
        )

    # Tests for a guest student user.

    def create_gueststudent(self, data={}):
        base = {
            'first_name': 'guest',
            'last_name': 'student',
        }
        return GuestStudent.objects.create(**dict(base, **data))

    def test_gueststudent_get_ok(self):
        """
        GuestStudentForm is displayed for a guest student.
        """
        gueststudent = self.create_gueststudent()

        self.client.force_login(gueststudent)
        r = self.client.get(self.url)

        self.assertGetOk(r, GuestStudentForm)

    def test_gueststudent_post_ok(self):
        """
        A guest student can change his data.
        """
        gueststudent = self.create_gueststudent()

        self.client.force_login(gueststudent)
        r = self.client.post(self.url, {
            'first_name': 'visitor',
            'last_name': 'student',
            'birthdate': '',
            'home_country': 'Country',
            'email': 'another@email.net',
            'phone': '+331 23 45 67 89',
            'address': '',
            'administrative_status': '',
            'healthcare_status': '',
            'transport': '',
            'subjects': [str(self.subject.pk)],
            'committee_wishes': [],
            'private_remarks': '',
            'public_remarks': '',
        })

        self.assertPostOk(r)

        gueststudent.refresh_from_db()

        self.assertEqual(gueststudent.first_name, 'visitor')
        self.assertEqual(gueststudent.email, 'another@email.net')
        self.assertEqual(gueststudent.phone, '+331 23 45 67 89')
        self.assertEqual(gueststudent.home_country, 'Country')
        self.assertQuerysetEqual(
            gueststudent.subjects.all(),
            map(repr, [self.subject]),
        )

    # Tests for a tutor user.

    def create_tutor(self, data={}):
        base = {
            'first_name': 'great',
            'last_name': 'tutor',
        }
        return Tutor.objects.create(**dict(base, **data))

    def test_tutor_get_ok(self):
        """
        TutorForm is displayed for a tutor.
        """
        tutor = self.create_tutor()

        self.client.force_login(tutor)
        r = self.client.get(self.url)

        self.assertGetOk(r, TutorForm)

    def test_tutor_post_ok(self):
        """
        A tutor can change his data.
        """
        tutor = self.create_tutor()

        self.client.force_login(tutor)
        r = self.client.post(self.url, {
            'first_name': 'super',
            'last_name': 'volunteer',
            'email': 'another@email.net',
            'subjects': [str(self.subject.pk)],
        })

        self.assertPostOk(r)

        tutor.refresh_from_db()

        self.assertEqual(tutor.first_name, 'super')
        self.assertEqual(tutor.email, 'another@email.net')
        self.assertQuerysetEqual(
            tutor.subjects.all(),
            map(repr, [self.subject]),
        )

    # Tests for a volunteer user.

    def create_volunteer(self, data={}):
        base = {
            'first_name': 'great',
            'last_name': 'volunteer',
        }
        return Volunteer.objects.create(**dict(base, **data))

    def test_volunteer_get_ok(self):
        """
        VolunteerForm is displayed for a volunteer.
        """
        volunteer = self.create_volunteer()

        self.client.force_login(volunteer)
        r = self.client.get(self.url)

        self.assertGetOk(r, VolunteerForm)

    def test_volunteer_post_ok(self):
        """
        A volunteer can change his data.
        """
        volunteer = self.create_volunteer()

        self.client.force_login(volunteer)
        r = self.client.post(self.url, {
            'first_name': 'super',
            'last_name': 'volunteer',
            'email': 'another@email.net',
            'phone': '+331 23 45 67 89',
            'subjects': [str(self.subject.pk)],
            'languages': [],
            'language_wishes': [],
            'commitee_wishes': [],
            'position_wishes': [],
        })

        self.assertPostOk(r)

        volunteer.refresh_from_db()

        self.assertEqual(volunteer.first_name, 'super')
        self.assertEqual(volunteer.email, 'another@email.net')
        self.assertEqual(volunteer.phone, '+331 23 45 67 89')
        self.assertQuerysetEqual(
            volunteer.subjects.all(),
            map(repr, [self.subject]),
        )


class VolunteerApprovalTest(TestCase):
    def test_decorator(self):
        vol = Volunteer.objects.create(
            username="vol",
            first_name="great",
            last_name="volunteer"
        )
        guest = GuestStudent.objects.create(
            username="guest",
            first_name="great",
            last_name="student"
        )
        tut = Tutor.objects.create(
            username="tut",
            first_name="great",
            last_name="tutor"
        )

        self.assertFalse(is_approved_user(AnonymousUser()))
        self.assertTrue(is_approved_user(guest))
        self.assertTrue(is_approved_user(tut))
        self.assertFalse(is_approved_user(vol))  # not approved yet
        vol.approved = True
        vol.save()
        vol.volunteer.refresh_from_db()
        self.assertTrue(is_approved_user(vol))  # now approved


class SignupView(TestCase):
    """
    The signup view is served by django-allauth, but uses our custom
    SignupForm.
    """
    def setUp(self):
        self.url = '/auth/signup/'

    def test_url(self):
        self.assertEqual(reverse('account_signup'), self.url)

    def test_get(self):
        r = self.client.get(self.url)
        self.assertEqual(r.status_code, 200)

    def test_post(self):
        r = self.client.post(self.url, {
            'username': 'username',
            'password1': 'thepassword',
            'password2': 'thepassword',
        })

        self.assertEqual(r.status_code, 302)
        u = User.objects.get(username='username')
        self.assertTrue(hasattr(u, 'volunteer'))
        # A new volunteer should not be approved.
        self.assertFalse(u.volunteer.approved)
