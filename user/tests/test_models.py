from django.contrib.auth import get_user_model
from django.core import mail
from django.core.management import call_command
from django.test import TestCase

from user.models import GuestStudent, Tutor, Volunteer

User = get_user_model()


class SendMailOnContactChangeTestsMixin:

    def assertContactMailSent(self, to):
        """
        Perform the following checks:
        - Recipients of mails in outbox are identical to `to`.
        - Each recipient in `to` received an individual mail.
        - Mail subjects contains 'contact'.
        """
        self.assertEqual(len(to), len(mail.outbox))
        for m in mail.outbox:
            self.assertEqual(len(m.to), 1)
            self.assertIn('contact', m.subject)

    def check_send_mail_on_contact_change(self, instance, expected_to):
        self.check_send_mail_on_field_change(
            instance, 'email', 'plop@plop.net', '', expected_to)
        self.check_send_mail_on_field_change(
            instance, 'phone', '0123456789', '', expected_to)

    def check_send_mail_on_field_change(
        self, instance, field, value, blank_value,
        expected_to,
    ):
        # Init blank.
        setattr(instance, field, blank_value)
        instance.save()

        # Notifications are sent when email changes.
        mail.outbox = []
        setattr(instance, field, value)
        instance.save()
        self.assertContactMailSent(expected_to)

        # No notification is sent when email doesn't change.
        mail.outbox = []
        setattr(instance, field, value)
        instance.save()
        self.assertContactMailSent([])

        # No notification is sent when email goes blank.
        mail.outbox = []
        setattr(instance, field, blank_value)
        instance.save()
        self.assertContactMailSent([])


class GuestStudentTests(SendMailOnContactChangeTestsMixin, TestCase):

    def test_send_mail_on_contact_change(self):
        call_command('syncmails', '-v0', 'shared/fixtures/custommail.json')

        gs1 = GuestStudent.objects.create(username='gs1', email='gs1@pei.net')
        tut1 = Tutor.objects.create(username='tut1', email='tut1@pei.net')
        vol1 = Volunteer.objects.create(username='vol1', email='vol1@pei.net')
        vol2 = Volunteer.objects.create(username='vol2', email='vol2@pei.net')

        # Perform checks using gs1.

        self.check_send_mail_on_contact_change(gs1, [])

        gs1.binome = vol1
        gs1.save()
        self.check_send_mail_on_contact_change(gs1, [vol1.email])

        gs1.tandem = vol2
        gs1.save()
        self.check_send_mail_on_contact_change(gs1, [vol1.email, vol2.email])

        # Tutors are not notified of contact changes.
        gs1.tuteur = tut1
        gs1.save()
        self.check_send_mail_on_contact_change(gs1, [vol1.email, vol2.email])


class TutorTests(SendMailOnContactChangeTestsMixin, TestCase):

    def test_send_mail_on_contact_change(self):
        call_command('syncmails', '-v0', 'shared/fixtures/custommail.json')

        gs1 = GuestStudent.objects.create(username='gs1', email='gs1@pei.net')
        gs2 = GuestStudent.objects.create(username='gs2', email='gs2@pei.net')
        tut1 = Tutor.objects.create(username='tut1', email='tut1@pei.net')
        vol1 = Volunteer.objects.create(username='vol1', email='vol1@pei.net')
        vol2 = Volunteer.objects.create(username='vol2', email='vol2@pei.net')
        vol3 = Volunteer.objects.create(username='vol3', email='vol3@pei.net')

        # Perform checks using tut1.

        self.check_send_mail_on_contact_change(tut1, [])

        gs1.tuteur = tut1
        gs1.save()
        self.check_send_mail_on_contact_change(tut1, [gs1.email])

        gs1.binome = vol1
        gs1.save()
        self.check_send_mail_on_contact_change(tut1, [gs1.email, vol1.email])

        gs1.tandem = vol2
        gs1.save()
        self.check_send_mail_on_contact_change(tut1, [
            gs1.email, vol1.email, vol2.email,
        ])

        gs2.binome = vol3
        gs2.tuteur = tut1
        gs2.save()
        self.check_send_mail_on_contact_change(tut1, [
            gs1.email, vol1.email, vol2.email,
            gs2.email, vol3.email,
        ])

        # If a user is connected twice to the user whose contact data change,
        # he should not receive duplicate mails.
        gs2.tandem = vol1
        gs2.save()
        self.check_send_mail_on_contact_change(tut1, [
            gs1.email, vol1.email, vol2.email,
            gs2.email, vol3.email,
        ])


class VolunteerTests(SendMailOnContactChangeTestsMixin, TestCase):

    def test_send_mail_on_contact_change(self):
        call_command('syncmails', '-v0', 'shared/fixtures/custommail.json')

        gs1 = GuestStudent.objects.create(username='gs1', email='gs1@pei.net')
        gs2 = GuestStudent.objects.create(username='gs2', email='gs2@pei.net')
        gs3 = GuestStudent.objects.create(username='gs3', email='gs3@pei.net')
        tut1 = Tutor.objects.create(username='tut1', email='tut1@pei.net')
        vol1 = Volunteer.objects.create(username='vol1', email='vol1@pei.net')
        vol2 = Volunteer.objects.create(username='vol2', email='vol2@pei.net')
        vol3 = Volunteer.objects.create(username='vol3', email='vol3@pei.net')

        # Perform checks using vol1.

        self.check_send_mail_on_contact_change(vol1, [])

        gs1.binome = vol1
        gs1.save()
        self.check_send_mail_on_contact_change(vol1, [gs1.email])

        gs1.tandem = vol2
        gs1.save()
        self.check_send_mail_on_contact_change(vol1, [gs1.email, vol2.email])

        # Tutors are not notified of contact changes.
        gs1.tuteur = tut1
        gs1.save()
        self.check_send_mail_on_contact_change(vol1, [gs1.email, vol2.email])

        gs2.binome = vol3
        gs2.tandem = vol1
        gs2.save()
        self.check_send_mail_on_contact_change(vol1, [
            gs1.email, vol2.email,
            gs2.email, vol3.email,
        ])

        # If a user is connected twice to the user whose contact data change,
        # he should not receive duplicate mails.
        gs3.binome = vol1
        gs3.tandem = vol3
        gs3.save()
        self.check_send_mail_on_contact_change(vol1, [
            gs1.email, vol2.email,
            gs2.email, vol3.email,
            gs3.email,
        ])


class VolunteerQuerySetTests(TestCase):

    def test_binome_of(self):
        vol1 = Volunteer.objects.create(username='vol1')
        vol2 = Volunteer.objects.create(username='vol2')
        vol3 = Volunteer.objects.create(username='vol3')

        gs1 = GuestStudent.objects.create(username='gs1', binome=vol1)
        GuestStudent.objects.create(username='gs2', binome=vol1)
        GuestStudent.objects.create(username='gs3', binome=vol2)

        self.assertQuerysetEqual(
            Volunteer.objects.binome_of(),
            map(repr, [vol1, vol2]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.binome_of([gs1]),
            map(repr, [vol1]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.binome_of(exclude=True),
            map(repr, [vol3]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.binome_of([gs1], exclude=True),
            map(repr, [vol2, vol3]),
        )

    def test_tandem_of(self):
        vol1 = Volunteer.objects.create(username='vol1')
        vol2 = Volunteer.objects.create(username='vol2')
        vol3 = Volunteer.objects.create(username='vol3')

        gs1 = GuestStudent.objects.create(username='gs1', tandem=vol1)
        GuestStudent.objects.create(username='gs2', tandem=vol1)
        GuestStudent.objects.create(username='gs3', tandem=vol2)

        self.assertQuerysetEqual(
            Volunteer.objects.tandem_of(),
            map(repr, [vol1, vol2]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.tandem_of([gs1]),
            map(repr, [vol1]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.tandem_of(exclude=True),
            map(repr, [vol3]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.tandem_of([gs1], exclude=True),
            map(repr, [vol2, vol3]),
        )

    def test_of_gueststudents(self):
        vol1 = Volunteer.objects.create(username='vol1')
        vol2 = Volunteer.objects.create(username='vol2')
        vol3 = Volunteer.objects.create(username='vol3')

        gs1 = GuestStudent.objects.create(
            username='gs1', binome=vol2, tandem=vol1)
        GuestStudent.objects.create(
            username='gs2', binome=vol1, tandem=vol1)
        GuestStudent.objects.create(
            username='gs3', tandem=vol2)

        self.assertQuerysetEqual(
            Volunteer.objects.of_gueststudents(),
            map(repr, [vol1, vol2]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.of_gueststudents([gs1]),
            map(repr, [vol1, vol2]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.of_gueststudents(exclude=True),
            map(repr, [vol3]),
        )
        self.assertQuerysetEqual(
            Volunteer.objects.of_gueststudents([gs1], exclude=True),
            map(repr, [vol3]),
        )
