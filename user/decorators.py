from django.contrib.auth.decorators import user_passes_test


def is_approved_user(user):
    """
    If a user is not logged in or is a not-yet-approved volunteer, they are not
    approved.
    """
    if not user.is_authenticated:
        return False
    return (
        user.volunteer.approved if user.is_volunteer
        else True
    )


approvement_required = user_passes_test(is_approved_user)
