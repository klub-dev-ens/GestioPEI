from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

class User(AbstractUser):
    phone = models.CharField(
        _('numéro de téléphone'),
        max_length=20,
        blank=True,
    )
    birthdate = models.DateField(
        _('date de naissance'),
        blank=True, null=True,
    )
    address = models.TextField(
        _('adresse postale'),
        blank=True,
    )

    objects = UserManager()

    class Meta:
        verbose_name = _("utilisateur")
        verbose_name_plural = _("utilisateurs")
        ordering = ('last_name', 'first_name')

    def __str__(self):
        return self.get_full_name() or self.username

    # The three following properties are shortcuts to determine user's type and
    # provide abstraction of data layer.

    @property
    def is_gueststudent(self):
        return hasattr(self, 'gueststudent')

    @property
    def is_tutor(self):
        return hasattr(self, 'tutor')

    @property
    def is_volunteer(self):
        return hasattr(self, 'volunteer')

    def specific(self):
        if self.is_gueststudent:
            return self.gueststudent
        if self.is_tutor:
            return self.tutor
        if self.is_volunteer:
            return self.volunteer
        return self

    def get_pei_relationship(self, target):
        if self.is_superuser:
            return "admin"
        if self == target:
            return "self"
        elif target.is_gueststudent:
            if self.is_volunteer:
                if target.gueststudent.binome == self.volunteer:
                    return "binome"
                elif target.gueststudent.tandem == self.volunteer:
                    return "tandem"
            elif self.is_tutor:
                if target.gueststudent.tutor == self.tutor:
                    return "tutor" # TODO tuteur oui ?
        elif target.is_volunteer:
            if self.is_gueststudent:
                if self.gueststudent.binome == target.volunteer:
                    return "binome"
                elif self.gueststudent.tandem == target.volunteer:
                    return "tandem"
        return None

    def get_absolute_url(self):
        return reverse("user:profile.show", kwargs={"username":self.username})

class GuestStudent(User):
    ADMINISTRATIVE_STATUS = (
        ('DA', _("demandeur d'asile")),
        ('RS', _("réfugié statutaire")),
        ('PS', _("protection subsidiaire")),
        ('AP', _("apatride")),
        ('A', _("autre")),
    )
    HEALTHCARE_STATUS = (
        ('CMU', _("couverture médicale universelle")),
        ('AME', _("aide médicale d'état")),
        ('NONE', _("aucune aide aux soins")),
    )
    TRANSPORT = (
        ('FREE', _("pass navigo gratuit")),
        ('FS', _("forfait solidarité")),
        ('NORMAL', _("forfait à 73€/mois")),
        ('NONE', _("pas de forfait")),
    )
    FRENCH_LEVEL = (
        ('A1', "A1"),
        ('A2', "A2"),
        ('B1', "B1"),
        ('B2', "B2"),
    )
    CARD_STATUS = (
        ('ND', _("non demandée")),
        ('DC', _("demande en cours")),
        ('R', _("reçue mais non distribuée")),
        ('D', _("distribuée")),
    )
    TICKET_STATUS = (
        ('ND', _("non demandés")),
        ('DC', _("demande en cours")),
        ('R', _("reçus mais non distribués")),
        ('D', _("distribués")),
    )
    subjects = models.ManyToManyField(
        'shared.Subject',
        verbose_name=_("disciplines"),
    )
    home_country = models.CharField(
        _("pays d'origine"),
        max_length=255,
        blank=True,
    )
    administrative_status = models.CharField(
        _("statut administratif"),
        max_length=255,
        choices=ADMINISTRATIVE_STATUS,
        blank=True,
    )
    healthcare_status = models.CharField(
        _("prise en charge médicale"),
        max_length=255,
        choices=HEALTHCARE_STATUS,
        blank=True,
    )
    card_status = models.CharField(
        _("Carte ENS"),
        max_length=255,
        choices=CARD_STATUS,
        blank=True,
    )
    meal_ticket_status = models.CharField(
        _("tickets restaurant"),
        max_length=255,
        choices=TICKET_STATUS,
        blank=True,
    )
    transport = models.CharField(
        _("forfait de transport"),
        max_length=255,
        choices=TRANSPORT,
        blank=True,
    )
    staff_remarks = models.TextField(
        _("remarques de l'équipe"),
        blank=True,
    )
    public_remarks = models.TextField(
        _("remarques publiques"),
        blank=True,
        help_text=_("remarques de l'étudiant·e visibles par le binôme"),
    )
    private_remarks = models.TextField(
        _("remarques privées"),
        blank=True,
        help_text=_("remarques de l'étudiant·e visibles uniquement par "
                    "les administrat·rices·eurs"),
    )
    committee_wishes = models.ManyToManyField(
        'Committee',
        verbose_name=_("comités souhaités"),
        blank=True,
    )
    french_level = models.CharField(
        _("niveau de français"),
        max_length=255,
        choices=FRENCH_LEVEL,
        blank=True,
    )
    tuteur = models.ForeignKey(
        'Tutor',
        verbose_name=_("tuteur"),
        blank=True, null=True,
    )
    tandem = models.ForeignKey(
        'Volunteer',
        verbose_name=_("tandem"),
        related_name='tandem',
        blank=True, null=True,
    )
    binome = models.ForeignKey(
        'Volunteer',
        verbose_name=_("binome"),
        related_name='binome',
        blank=True, null=True,
    )
    languages = models.ManyToManyField(
        'shared.Language',
        verbose_name=_("langues parlées"),
    )

    # This manager is required to make this model correctly works, e.g. with
    # 'ModelForm's.
    objects = UserManager()

    class Meta:
        verbose_name = _("étudiant invité")
        verbose_name_plural = _("étudiants invités")


class VolunteerQuerySet(models.QuerySet):
    def binome_of(self, gueststudents=None, exclude=False):
        """
        Only keep Volunteers who are 'binome' of 'gueststudents' argument.

        If 'gueststudents' is not given, keep Volunteers who are 'binome' of
        any guest student.

        If 'exclude' argument is True, it skip these Volunteers. Its default
        value is False.
        """
        return self.of_gueststudents(
            positions=['binome'],
            gueststudents=gueststudents, exclude=exclude,
        )

    def tandem_of(self, gueststudents=None, exclude=False):
        """
        Only keep Volunteers who are 'tandem' of 'gueststudents' argument.

        If 'gueststudents' is not given, keep Volunteers who are 'tandem' of
        any guest student.

        If 'exclude' argument is True, it skip these Volunteers. Its default
        value is False.
        """
        return self.of_gueststudents(
            positions=['tandem'],
            gueststudents=gueststudents, exclude=exclude,
        )

    def of_gueststudents(self, gueststudents=None, exclude=False,
                         positions=None):
        if positions is None:
            positions = ['binome', 'tandem']

        filters = Q()
        if 'binome' in positions:
            filters |= (
                Q(binome__isnull=False) if gueststudents is None
                else Q(binome__in=gueststudents)
            )
        if 'tandem' in positions:
            filters |= (
                Q(tandem__isnull=False) if gueststudents is None
                else Q(tandem__in=gueststudents)
            )

        if exclude:
            return self.exclude(filters)
        return self.filter(filters).distinct()


# Inheritance from UserManager is required for the Volunteer model to function
# properly, e.g. with 'ModelForm's.

class VolunteerManager(UserManager.from_queryset(VolunteerQuerySet)):
    """
    FYI: Why is it a subclass ?
    https://docs.djangoproject.com/en/1.11/topics/migrations/#model-managers
    """


class Volunteer(User):
    languages = models.ManyToManyField(
        'shared.Language',
        verbose_name=_("langues parlées"),
        related_name='spoken_vol',
        blank=True,
    )
    language_wishes = models.ManyToManyField(
        'shared.Language',
        verbose_name=_("langues souhaitées"),
        related_name='wishes_vol',
        blank=True,
    )
    subjects = models.ManyToManyField(
        'shared.Subject',
        verbose_name=_("disciplines"),
    )
    committee_wishes = models.ManyToManyField(
        'Committee',
        verbose_name=_("comités souhaités"),
        blank=True,
    )
    position_wishes = models.ManyToManyField(
        'Position',
        verbose_name=_("positions souhaitées"),
        blank=True,
    )
    approved = models.BooleanField(
        _("Approuvé·e par les administrat·rice·eur·s"),
        default=False
    )

    objects = VolunteerManager()

    class Meta:
        verbose_name = _("bénévole")
        verbose_name_plural = _("bénévoles")


class Tutor(User):
    subjects = models.ManyToManyField(
        'shared.Subject',
        verbose_name=_("disciplines"),
    )

    # This manager is required to make this model correctly works, e.g. with
    # 'ModelForm's.
    objects = UserManager()

    class Meta:
        verbose_name = _("tuteur")
        verbose_name_plural = _("tuteurs")


class Position(models.Model):
    name = models.CharField(
        _("nom"),
        max_length=255,
    )

    class Meta:
        verbose_name = _("position")
        verbose_name_plural = _("positions")

    def __str__(self):
        return self.name


class Committee(models.Model):
    name = models.CharField(
        _("nom"),
        max_length=255
    )
    description = models.TextField(
        _('description'),
        blank=True,
    )
    members = models.ManyToManyField(
        User,
        verbose_name=_("membres"),
        blank=True,
    )

    class Meta:
        verbose_name = _("comité")
        verbose_name_plural = _("comités")

    def __str__(self):
        return self.name
