from django.contrib.auth.mixins import UserPassesTestMixin

from .decorators import is_approved_user


class ApprovalRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        return is_approved_user(self.request.user)
