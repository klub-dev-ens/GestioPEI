from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from shared.utils.admin import ModelAdmin

from allauth import app_settings as allauth_settings
from allauth.account.models import EmailAddress
from allauth.socialaccount.models import SocialAccount

from .forms import AuthSubUserAdminForm
from .models import Committee, GuestStudent, Tutor, User, Volunteer


class PositionFilter(admin.SimpleListFilter):
    """
    Filter for Volunteer querysets.

    The choices are the positions a Volunteer can take with a GuestStudent,
    namely 'binome' or 'tandem'.

    Queryset filtering only keeps Volunteers who occupy the selected position
    for at least one GuestStudent.
    Select the empty value to return Volunteers who do not occupy any
    positions with GuestStudents.
    """
    title = _("positions occupées")
    parameter_name = 'positions'

    def lookups(self, request, model_admin):
        return (
            ('any', _("Au moins une")),
            ('binome', _("Binôme")),
            ('tandem', _("Tandem")),
            ('none', _("Aucune")),
        )

    def queryset(self, request, queryset):
        if self.value() == 'any':
            return queryset.of_gueststudents()
        if self.value() == 'binome':
            return queryset.binome_of()
        if self.value() == 'tandem':
            return queryset.tandem_of()
        if self.value() == 'none':
            return queryset.of_gueststudents(exclude=True)


class AuthSubUserAdminMixin:
    form = AuthSubUserAdminForm


@admin.register(GuestStudent)
class GuestStudentAdmin(AuthSubUserAdminMixin, ModelAdmin):
    # List

    list_display = ('__str__', 'email', 'phone', 'tuteur', 'tandem', 'binome')
    list_filter = (
        'subjects', 'languages', 'french_level', 'administrative_status',
        'healthcare_status', 'meal_ticket_status', 'card_status',
    )
    list_select_related = ('tuteur', 'tandem', 'binome')

    search_fields = (
        'username', 'first_name', 'last_name', 'email',
        'tuteur__first_name', 'tuteur__last_name',
        'tandem__first_name', 'tandem__last_name',
        'binome__first_name', 'binome__last_name',
    )

    # Forms

    fieldsets = (
        (None, {
            'fields': (
                'username', ('first_name', 'last_name'), 'birthdate',
                'home_country',
            ),
        }),
        (_("Contact"), {
            'fields': (('email', 'phone'), 'address'),
        }),
        (_("Suivi effectué par"), {
            'fields': ('tuteur', 'tandem', 'binome'),
        }),
        (_("Compétences"), {
            'fields': ('subjects', 'languages', 'french_level'),
        }),
        (_("Administratif"), {
            'fields': (
                'administrative_status', 'healthcare_status',
                'meal_ticket_status', 'card_status',
            ),
        }),
        (_("Compléments"), {
            'fields': (
                'public_remarks', 'private_remarks', 'staff_remarks',
            ),
        }),
    )

    filter_horizontal = ('languages',)

    radio_fields = {
        'french_level': admin.HORIZONTAL,
        'administrative_status': admin.HORIZONTAL,
        'healthcare_status': admin.HORIZONTAL,
        'meal_ticket_status': admin.HORIZONTAL,
        'card_status': admin.HORIZONTAL,
    }


@admin.register(Tutor)
class TutorAdmin(AuthSubUserAdminMixin, ModelAdmin):
    # List

    list_display = ('__str__', 'email')
    list_filter = ('subjects',)

    search_fields = ('username', 'first_name', 'last_name', 'email')

    # Forms

    fields = ('username', ('first_name', 'last_name'), 'email', 'subjects')


@admin.register(Volunteer)
class VolunteerAdmin(AuthSubUserAdminMixin, ModelAdmin):
    # List

    list_display = ('__str__', 'email', 'phone')
    list_filter = (
        PositionFilter, 'languages', 'subjects', 'language_wishes',
        'committee_wishes', 'position_wishes',
    )

    search_fields = ('username', 'first_name', 'last_name', 'email')

    # Forms

    fieldsets = (
        (None, {
            'fields': (
                'username', ('first_name', 'last_name'), 'email', 'phone',
                'approved',
            ),
        }),
        (_("Compétences"), {
            'fields': ('languages', 'subjects'),
        }),
        (_("Souhaits"), {
            'fields': (
                'language_wishes', 'committee_wishes', 'position_wishes',
            ),
        }),
    )

    filter_horizontal = ('languages', 'language_wishes')


class CommitteeUserInline(admin.TabularInline):
    model = Committee.members.through

    verbose_name = _("membre")
    verbose_name_plural = _("membres")


@admin.register(Committee)
class CommitteeAdmin(ModelAdmin):
    # List

    search_fields = ('name', 'members__first_name', 'members__last_name')

    # Forms

    fields = ('name',)

    inlines = [
        CommitteeUserInline,
    ]

# Drop some allauth models:
# - EmailAddress is not used
# - SocialAccount can be changed from UserAdmin.

admin.site.unregister(EmailAddress)

if allauth_settings.SOCIALACCOUNT_ENABLED:
    admin.site.unregister(SocialAccount)


class SocialAccountInline(admin.TabularInline):
    model = SocialAccount
    fields = ('user', 'provider', 'uid')


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    inlines = DjangoUserAdmin.inlines.copy()

    if allauth_settings.SOCIALACCOUNT_ENABLED:
        inlines.append(SocialAccountInline)
