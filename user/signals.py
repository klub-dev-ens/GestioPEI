from django.contrib.auth import get_user_model
from django.db.models.signals import pre_save
from django.dispatch import receiver

from .utils import send_mail_contact_change

User = get_user_model()


@receiver(pre_save)
def send_notif_on_contact_change(sender, instance, raw=False, **kwargs):
    """
    When contact data of a user are changed, notify these changes by mails.

    - At least one contact field value must be updated to a non-empty value.
    - Do nothing if `instance` is being created.

    Recipient list is got from `get_notifs_contact_change`.

    This uses the mail template named `user-change-contact`, managed by
    `custommail`.
    """
    if not issubclass(sender, User) or instance.pk is None or raw:
        return

    old = User.objects.get(pk=instance.pk)

    contact_fields = ['email', 'phone']
    # Contact field names whose values change and are not empty.
    changed_fields = []

    for f in contact_fields:
        old_v = getattr(old, f)
        new_v = getattr(instance, f)
        if old_v != new_v and new_v:
            changed_fields.append(f)

    if not changed_fields:
        # Contact fields remain unchanged. Do not notify.
        return

    send_mail_contact_change(instance)
