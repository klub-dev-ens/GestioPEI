from allauth.account.adapter import DefaultAccountAdapter

from .models import Volunteer


class AccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return True

    def save_user(self, request, user, form, commit=True):
        """Saves a new User using signup form."""
        user = super().save_user(request, user, form, commit=True)

        volunteer = Volunteer(user_ptr=user)
        volunteer.__dict__.update(user.__dict__)
        volunteer.save()
