from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView, DetailView
from django.views.generic.edit import FormView

from .forms import GuestStudentForm, TutorForm, UserForm, VolunteerForm
from .models import User

class ProfileView(LoginRequiredMixin, DetailView):
    model = User

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.relationship = self.request.user.get_pei_relationship(self.object)
        if self.relationship is None or self.relationship == "tutor":
            return self.handle_no_permission()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)
    
    def get_object(self):
        if self.kwargs.get('username'):
            return get_object_or_404(User, username=self.kwargs.get('username'))
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['user_object'] = context['object']
        context['object'] = context['object'].specific()
        context['relationship'] = self.relationship
        return context
        
    def get_template_names(self):
        if self.object.is_volunteer:
            return ['user/profile_volunteer.html']
        elif self.object.is_gueststudent:
            return ['user/profile_gueststudent.html']
        elif self.object.is_tutor:
            return ['user/profile_tutor.html']
        else:
            return ['user/profile.html']

profile_view = ProfileView.as_view()


class ProfileEditView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    """
    Connected user profile edition view.

    Note
        To simplify this view, a user must only be one of the following:
            GuestStudent, Tutor, Volunteer. If this restriction is annoying,
            one should rewrite this view (e.g. with multiple forms).

    """
    template_name = 'user/profile_edit.html'
    success_url = reverse_lazy('user:profile')
    success_message = _("Profil mis à jour.")

    def get_form_class(self):
        user = self.request.user
        if user.is_gueststudent:
            return GuestStudentForm
        elif user.is_tutor:
            return TutorForm
        elif user.is_volunteer:
            return VolunteerForm
        return UserForm

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        user = self.request.user
        if user.is_gueststudent:
            instance = user.gueststudent
        elif user.is_tutor:
            instance = user.tutor
        elif user.is_volunteer:
            instance = user.volunteer
        else:
            instance = user
        form_kwargs['instance'] = instance
        return form_kwargs

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


profile_edit_view = ProfileEditView.as_view()
