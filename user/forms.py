from django import forms
from django.urls import reverse
from django.utils.translation import ugettext as _

from .models import GuestStudent, Tutor, User, Volunteer


class GuestStudentForm(forms.ModelForm):
    class Meta:
        model = GuestStudent
        fields = (
            'first_name', 'last_name', 'birthdate', 'home_country',
            'email', 'phone', 'address',
            'administrative_status', 'healthcare_status', 'transport',
            'subjects', 'committee_wishes',
            'public_remarks', 'private_remarks',
        )


class TutorForm(forms.ModelForm):
    class Meta:
        model = Tutor
        fields = ('first_name', 'last_name', 'email', 'subjects')


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class VolunteerForm(forms.ModelForm):
    class Meta:
        model = Volunteer
        fields = (
            'first_name', 'last_name', 'email', 'phone',
            'subjects', 'languages',
            'language_wishes', 'committee_wishes', 'position_wishes',
        )


class AuthSubUserAdminForm(forms.ModelForm):
    """
    Add an extra help message to the `username` field with a link to change
    the user data related to authentication and authorizations: password,
    permissions, groups memberships, social accounts…

    If instance is being created, only notify user that these changes will be
    available once it is created.

    No Meta class is defined, as it is meant to be used from `ModelAdmin` which
    already manages this.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pk = self.instance.pk
        if pk is None:
            extra_auth_help = _(
                "Vous aurez la possibilité de modifier les informations "
                "concernant l'accès à ce site une fois l'utilisateur·rice "
                "créé·e."
            )
        else:
            extra_auth_help = _(
                "Suivez <a href=\"{}\" target=\"_blank\">ce lien</a> pour "
                "afficher et modifier les informations concernant l'accès à "
                "ce site de cet·te utilisateur·rice."
                .format(reverse('admin:user_user_change', args=(pk,)))
            )

        if self.fields['username'].help_text:
            self.fields['username'].help_text += '<br>' + extra_auth_help
        else:
            self.fields['username'].help_text += extra_auth_help
