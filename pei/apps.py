from django.apps import AppConfig


class PeiConfig(AppConfig):
    name = 'pei'
    verbose_name = "PEI"
