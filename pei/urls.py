from django.conf.urls import url
from .views import PeiHome

app_name = 'pei'
urlpatterns = [
    url(r'^$', PeiHome.as_view(), name="home"),
]
